import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as spint
import math

def clipper(Vo,t,Vi,Vt,Is,R,C):
    return (Vi-Vo)/(R*C)-2.0*Is*np.sinh(Vo/Vt)/C
    
R = 2.2E4
C = 1.0E-10
Is = 2.25E-9
Vt = 45.3E-3 

t = np.array([ i/100.0 for i in range(200)])
Vo0= t*0.0

amp = np.array([i/10.0 for i in range(-40,40,1)])

res = []

for j in amp:
    print j
    Vi = np.sin(t*400.0/(2.0*math.pi))+j
    Vi=t*0.0+j
    
    Vo=spint.odeint(clipper,Vo0,t,args=(Vi,Vt,Is,R,C,))

    M = np.amax(Vo)
    m = np.amin(Vo)
    
    res.append(M-m)

'''
Vi=amp

X,Y = np.meshgrid(t,t)

F = X/(R*C)
G = -Y/(R*C)-2.0*Is*np.sinh(Y/Vt)/C

plt.contour(X, Y, (F - G), [0])
plt.show()
'''

plt.xscale('linear')
plt.yscale('linear')

#plt.plot(x,filt*sig)
#plt.plot(x,10.0*np.log10(filt1))
#plt.plot(x,10.0*np.log10(filt2))
#plt.plot(x,10.0*np.log10(filt3))
plt.plot(t,Vi)
plt.plot(t,Vo[1])
#plt.plot(amp,res)

#plt.plot(x,sig)

plt.show()

