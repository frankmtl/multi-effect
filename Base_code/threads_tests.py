import sys
import math
import numpy as np
import time
import getopt
import alsaaudio
import struct
import matplotlib.pyplot as plt
import threading as T


def unpack_buf(l,buf, fmt, periodsize):
    if l:
        return np.array([struct.unpack('i',buf[4*i:4*(i+1)])[0] for i in range(l/4)])
    else:
        return np.array([ 0 for i in range(4*periodsize)])
    
def pack_buf(fmt,data):
    pac_buf = ''
    for i in data:
            pac_buf += struct.pack('<i',i)
    return pac_buf
    
def TodB(Signal):
    return 20.0*np.log10(np.absolute(Signal))
    
def ToLin(SignaldB):
    return np.power(10,SignaldB/20)
    
def compressor(SignaldB, Threshold=-10.0, Ratio=1.0, KneeWidth=5.0, AttackTime=0.5, ReleaseTime=1.0):
    '''
    compressor takes as input signal in dB and return compressed signal in dB
    
    Kneewidth: Knee width in dB, specified as a real scalar greater than or equal to 0.
    Threshold: Operation threshold in dB, specified as a real scalar.
    Ratio: Compression ratio, specified as a real scalar greater than or equal to 1.
    AttackTime: Attack time in seconds, specified as a real scalar greater than or equal to 0.
                Attack time is the time it takes the compressor gain to rise from 10% to 90% 
                of its final value when the input goes above the threshold.
    ReleaseTime: Release time in seconds, specified as a real scalar greater than or equal to 0.
                 Release time is the time it takes the compressor gain to drop from 90% to 10% 
                 of its final value when the input goes below the threshold. 
    '''
    
    Gain = []
    AttackCoeff = math.exp(-np.log(9)/(samplerate*AttackTime))
    ReleaseCoeff = math.exp(-np.log(9)/(samplerate*ReleaseTime))
#    Makeup = Threshold - (Threshold)/Ratio
    Makeup=0
    for i in SignaldB:
        if i<(Threshold - KneeWidth):
            Gain.append(i)
        elif i - Threshold >= math.fabs(KneeWidth/2.0):
            Gain.append(i + ((1.0/float(Ratio) - 1.0) * (i - Threshold + KneeWidth)**2)/2.0*float(KneeWidth))
        else:
            Gain.append(Threshold + (i-Threshold)*i/Ratio) 
    GainMod = np.array(Gain) - SignaldB
    GainSmoothed = []
    for i in GainMod:
        LastElement = len(GainSmoothed) - 1
        if (LastElement<0):
            GainSmoothed.append(ReleaseCoeff*i + (1- ReleaseCoeff)*math.fabs(i))
        else:            
            if (abs(i)<= GainSmoothed[LastElement]):
                GainSmoothed.append(ReleaseCoeff*GainSmoothed[LastElement] + (1- ReleaseCoeff)*math.fabs(i))
            else:
                GainSmoothed.append(AttackCoeff*GainSmoothed[LastElement] + (1- AttackCoeff)*math.fabs(i))
    
    GaindB = (np.array(GainSmoothed)-Makeup)
    return SignaldB*GaindB   

class get_audio(T.Thread):
    '''
    Get audio from input device a return length and buffer
    '''
    def __init__(self):
        T.Thread.__init__(self)
        
    def __run__(self):
        l, buf = inp.read()
        return (l,buf)
        
class audio_processing(T.Thread):
    '''
    Audio processing thread, must be populated with processing functions
    '''
    def __init__(self,):
        T.Thread.__init__(self)        
    def __run__(self):
        unp_buf = unpack_buf(l,buf_read,fmt, periodsize)
        
        data = unp_buf
#        plt.plot(processed_data)
#        data = compressor(data)
        
        buf_to_write = pack_buf(fmt,data)
        return buf_to_write

        
class play_audio(T.Thread):
    '''
    Send packed audio buffer to out device 
    '''
    def __init__(self):
        T.Thread.__init__(self)
        
    def __run__(self):
        out.write(packed_buf)
    
channels=0
samplerate = 44100
sampledepth = 16
periodsize = 100

inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL,'hw:0')

# Set attributes: Mono, 44100 Hz, 16 bit little endian samples

inp.setchannels(channels)
inp.setrate(samplerate)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)

inp.setperiodsize(periodsize)

out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, 0)

# Set attributes: Mono, 44100 Hz, 16 bit little endian frames
out.setchannels(channels)
out.setrate(samplerate)
out.setformat(alsaaudio.PCM_FORMAT_S16_LE)

out.setperiodsize(periodsize)

plt.ion()

data = []
data_unp = []
asd = []
audio_in = get_audio()
proc = audio_processing()
audio_out = play_audio()

audio_in.start()
proc.start()
audio_out.start()

fmt = 'i'

print 'recording...'
while True:
#    audio_in.join()
    l,buf_read = audio_in.__run__()
#    proc.join()
    packed_buf = proc.__run__()
#    audio_out.join()
    audio_out.__run__()
    

'''   
f = open('unpacked.dat', 'wb')
for i in data_unp:
    string = ''
    for j in i:
        string += ' ' +str(j)
#    print string
    f.write(string + '\n')
f.close()

f = open('bits.dat', 'wb')
for i in data:
   f.write(i)
f.close()    

plt.show()
'''
