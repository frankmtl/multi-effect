import time
import numpy as np
import math

def HighPass(x,fc):
    return 1j*x/(1j*x+float(fc))
    
a = np.array([i for i in range(1024)])

fc=600

for i in range(100):
    t1 = time.clock()
    HighPass(a,fc)
    HighPass(a,fc)
    HighPass(a,fc)
    t2 = time.clock()
    tot=t2-t1
    t1 = time.clock()
    1j*a/(1j*a+float(fc))*1j*a/(1j*a+float(fc))*1j*a/(1j*a+float(fc))
    t2 = time.clock()
    tot2=t2-t1
    print '3 HighPass processed in ',tot,'s, one line processed in ',tot2,'s'

'''
def compressor(Signal, Threshold=-10.0, Ratio=1.0, KneeWidth=5.0, AttackTime=0.5, ReleaseTime=1.0):
#   
    Kneewidth: Knee width in dB, specified as a real scalar greater than or equal to 0.
    Threshold: Operation threshold in dB, specified as a real scalar.
    Ratio: Compression ratio, specified as a real scalar greater than or equal to 1.
    AttackTime: Attack time in seconds, specified as a real scalar greater than or equal to 0.
                Attack time is the time it takes the compressor gain to rise from 10% to 90% 
                of its final value when the input goes above the threshold.
    ReleaseTime: Release time in seconds, specified as a real scalar greater than or equal to 0.
                 Release time is the time it takes the compressor gain to drop from 90% to 10% 
                 of its final value when the input goes below the threshold. 
# 
    
    SignaldB = 20.0*np.log10(np.absolute(Signal))
    Gain = []
    AttackCoeff = math.exp(-np.log(9)/(samplerate*AttackTime))
    ReleaseCoeff = math.exp(-np.log(9)/(samplerate*ReleaseTime))
    Makeup = Threshold - (Threshold)/Ratio
    for i in SignaldB:
        if i<(Threshold - KneeWidth):
            Gain.append(i)
        elif i - Threshold >= math.fabs(KneeWidth/2.0):
            Gain.append(i + ((1.0/float(Ratio) - 1.0) * (i - Threshold + KneeWidth)**2)/2.0*float(KneeWidth))
        else:
            Gain.append(Threshold + (i-Threshold)*i/Ratio) 
    GainMod = np.array(Gain) - SignaldB
    GainSmoothed = []
    for i in GainMod:
        LastElement = len(GainSmoothed) - 1
        if (LastElement<0):
            GainSmoothed.append(ReleaseCoeff*i + (1- ReleaseCoeff)*math.fabs(i))
        else:            
            if (abs(i)<= GainSmoothed[LastElement]):
                GainSmoothed.append(ReleaseCoeff*GainSmoothed[LastElement] + (1- ReleaseCoeff)*math.fabs(i))
            else:
                GainSmoothed.append(AttackCoeff*GainSmoothed[LastElement] + (1- AttackCoeff)*math.fabs(i))
            
    GainLin = np.power(10,(np.array(GainSmoothed)-Makeup)/20)
    return Signal*GainLin
    
    
Signal = [np.random.random_integers(-64000,64000) for i in range(200)]
samplerate =44000
Makeup = 0

for i in range(100):
    t1 = time.clock()
    c = compressor(Signal)
    t2 = time.clock()
    total2 = t2 - t1

    print 'Compressor: ',total2,'s'
'''
