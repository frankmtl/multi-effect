import numpy as np
import matplotlib.pyplot as plt
import math

def HighPass(x,fc):
    return 1j*x/(1j*x+float(fc))
    
def HighPassTest(x,fc):
#    fc*=2.0E1
    return np.power(1j*x,2)/(np.power(1j*x,2)- float(fc)**2)
        
def LowPass(x,fc):
    return float(fc)/(1j*x+float(fc))

def LowPassTest(x,fc):
    fc*=2.0E1
    return float(fc)**2/(np.power(1j*x,2)- float(fc)**2)
            
def HighPass2nd(x,fc):
    a = np.power(1j*x,2)
    return a/(a + fc*x/5.0E-5 + fc**2)
    
def LowPass2nd(x,fc):
    return fc**2/(np.power(1j*x,2) + fc*x/1.0E-3 + fc**2 )
'''
def LowPass2nd(x,fc,QualityFactor):
    return 1/(1-np.power(x/fc,2)+1j*x/(QualityFactor*fc)
'''
    

x = range(0,1000000)

x.pop(3)
x.pop(600)

x = np.array(x)

sig = [0 for i in x]

for i in range(4):
    sig[i+20] = 2
    
for i in range(4):
    sig[i+100] = 2

for i in range(4):
    sig[i+10000] = 2
    
    
sig = np.array(sig)
frec = 3.0
#filt = np.power(x,2)/((x+2*math.pi*3.0)*(x+2*math.pi*600.0))
filt1 = LowPass(x,frec)#,100.0)
''' First High Pass filter '''
filtA = HighPassTest(x,420.0)
''' Second High Pass filter '''
filtB = HighPassTest(x,50000.0)
''' Low Pass filter '''
filtC = LowPassTest(x,0.23)

#filt2 = LowPass(x,3.0)#,100.0)

#print filt

#filt = -20.0*np.log10(x)

#yfft = np.absolute(np.fft.fft(y))

plt.xscale('log')
plt.yscale('linear')

#plt.plot(x,filt*sig)
#plt.plot(x,10.0*np.log10(filt1))
#plt.plot(x,10.0*np.log10(filt2))
#plt.plot(x,10.0*np.log10(filt3))
plt.plot(x, 10.0* np.log10( np.array(filtA)*np.array(filtB)*np.array(filtC))+115.0)

#plt.plot(x,sig)

plt.show()
