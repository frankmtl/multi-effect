import sys
import time
import getopt
import alsaaudio as aa
import numpy as np
import matplotlib.pyplot as plt
import struct

rec_device='hw:0,1'
play_device='hw:0,1'

# Open the device in nonblocking capture mode. The last argument could
# just as well have been zero for blocking mode. Then we could have
# left out the sleep call in the bottom of the loop
inp = aa.PCM(type=aa.PCM_CAPTURE, mode=aa.PCM_NONBLOCK)
# Open the device in playback mode.
out = aa.PCM(type=aa.PCM_PLAYBACK, mode=aa.PCM_NONBLOCK)
# Set attributes: Mono, 44100 Hz, 16 bit little endian samples
inp.setchannels(1)
inp.setrate(44100)
inp.setformat(aa.PCM_FORMAT_S16_LE)
# Set attributes: Mono, 44100 Hz, 16 bit little endian frames
out.setchannels(1)
out.setrate(int(44100*1.0))
out.setformat(aa.PCM_FORMAT_S16_LE)
# The period size controls the internal number of frames per period.
# The significance of this parameter is documented in the ALSA api.
# For our purposes, it is suficcient to know that reads from the device
# will return this many frames. Each frame being 2 bytes long.
# This means that the reads below will return either 320 bytes of data
# or 0 bytes of data. The latter is possible because we are in nonblocking
# mode.
inp.setperiodsize(160)
# The period size controls the internal number of frames per period.
# The significance of this parameter is documented in the ALSA api.
out.setperiodsize(160)

f = open('record_live', 'wb')
fdata=[]

loops = 10000
while loopsdone < loops:
    loopsdone += 1
    # Read data from device
    t0=time.clock()
    print 'loops ',loops
    l, data = inp.read()
    t1=time.clock()
    print 'l ', l
    out.write(data)
    t2=time.clock()
    fdata.append(data)
    print 'reading time ',t1-t0,' writing time ',t2-t1

[f.write(i) for i in fdata]

'''
    if l:
        f.write(data)
        time.sleep(.001)
'''
